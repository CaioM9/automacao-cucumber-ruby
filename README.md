# README 
Projeto de Automação de Testes com Ruby, Cucumber e Selenium WebDriver

Motivacao para usar Selenium:

Selenium WebDriver é uma ferramenta opesource com uma base grande de conhecimento disponível 
na web e de fácil acesso.  Utilizei o selenium porque é a ferramenta de automação que eu mais
tenho conhecimento e tempo de trabalho, a base lógica da linguagem me proporcionou aprender 
mais sobre a Ruby e o Cucumber.

Recomendações:

•Eu recomendo utilizar no lugar do prompt nativo do Windows, utilizar o cmder Console Emulator, 
por nos proporcionar mais funções do que o prompt nativo.

•Eu recomendo utilizar o Notpad++ ao invés do bloco de notas nativo do Windows, porque o Notpad++ 
nos proporciona uma melhor maneabilidade dos dados e do código.

Links:

•Cmder Console Emulator: http://cmder.net/ 

•Notepad++: https://notepad-plus-plus.org/download

•Ruby 2.4.1-1 x64: https://rubyinstaller.org/downloads/

•Devkit 4.7.2 x64 : https://rubyinstaller.org/downloads/

-----------------------------------------------------------------------------------------
Instalacao:

Executar o rubyinstaller-2.4.1-1-x64.exe

Criar uma pasta no repositório do Ruby no C: e extrair o conteúdo do DevKit-mingw64-64-4.7.2 para a pasta

cmder:

Acessar: C:/Ruby24-x64/devkit

Executar: ruby dk.rb init - para criar o arquivo config.yml

Executar: ruby dk.rb install - instala o Ruby

O windows tem uma falha no protocolo na hora de baixar as gems, 
a forma mais rapida e pratica que encontrei para corrigir o erro.

Executar: gem sources -a http://rubygems.org/ - Adiciona o novo protocolo HTTP

Executar: gem sources -r https://rubygems.org - Remove o protocolo HTTPS

Executar: gem sources -u -Atualiza as gems

Executar: gem install bundler Instala o bundle

Acessar C:/

Executar: Code

Executar: cd code
 
Executar: md Cucumber_Test - Cria o diretorio de testes cucumber

Executar: cd Cucumber_Test - Acessa o diretorio C:/Code/Cucumber_Test

Executar: gem install cucumber - Instala o Cucumber

Executar: cucumber --init -Cria os diretorios para execucao de testes do cucumber

Executar: bundle init - Cria o Gemfile no diretorio

---------------------------------------------------------------------------------
Preparacao do Ambiente: Exercicio 1
---------------------------------------------------------------------------------

Dentro do Gemfile adicionar as seguintes dependencias:

# A sample Gemfile
source "http://rubygems.org"

# gem "rails"
gem 'selenium-webdriver', '~> 2.48.1'
gem 'rspec-expectations', '~> 3.3.1'
gem 'cucumber', '~> 2.1.0'

-----------------------------------------------------------------------------------

Criar uma feature dento do projeto > pasta feature

# language: pt

Funcionalidade: validar login na pagina the internet
	Sendo o administrador da pagina the internet
	Posso cadastrar usuarios validos e invalidos
	Para validar a correta acao do sistema
	
# Cenario 1

	Cenario: Validar a tentativa de login valido
	   Dado que eu tenha o link de acesso da pagina
     Quando informar "tomsmith" no campo username
		  E informa a "SuperSecretPassword!" no campo password
		  E clicar no botao "Login"
	      E sera apresentado a mensagem "You logged into a secure área!"
      Entao quando eu clicar no botao Logout devera ser apresentado a mensagem "You logged out of the secure área!"

# Cenario 2

	Cenario: Validar a tentativa de login invalido, sem preencher os campos username e password
	   Dado que eu tenha o link de acesso
     Quando eu tentar efetuar o login sem preencher os campos username e password
      Entao o sistema devera apresentar a mensagem "Your username is invalid!"

# Cenario 3

	Cenario: Validar a tentativa de login invalido, preenchendo apenas o campo username corretamente
	   Dado o link de acesso
     Quando eu tentar efetuar o login, apos preencher apenas o campo username "tomsmith"
      Entao o sistema devera apresentar a mensagem "Your username is invalid!"
	
# Cenario 4 obs: a mensagem de erro do campo password esta incorreta!

	Cenario: Validar a tentativa de login invalido, preenchendo apenas o campo password corretamente
	   Dado o link de acesso
     Quando eu tentar efetuar o login, apos preencher apenas o campo password "SuperSecretPassword!"
      Entao o sistema devera apresentar a mensagem "Your username is invalid!"

----------------------------------------------------------------------------------------------

Criar um arquivo .rb na pasta Steps_Definitions

# encoding: utf-8

# Cenario 1
Dado(/^que eu tenha o link de acesso da pagina$/) do
	@navegador = Selenium::WebDriver.for :firefox
	@navegador.manage.window.maximize
	@navegador.manage.timeouts.implicit_wait = 5
	@navegador.get 'https://the-internet.herokuapp.com/login'
end

Quando(/^informar "([^"]*)" no campo username$/) do |user|
	@navegador.find_element(:id, 'username').send_keys(user)	
end

Quando(/^informa a "([^"]*)" no campo password$/) do |password|
	@navegador.find_element(:id, 'password').send_keys(password)  
end

Quando(/^clicar no botao "([^"]*)"$/) do |arg1|
	@navegador.find_element(:css, 'button[type="submit"]').click
end

Quando(/^sera apresentado a mensagem "([^"]*)"$/) do |menLogin|                  
	mensagemLogin = @navegador.find_element(:id, 'flash').text
	expect(mensagemLogin)==(menLogin)
end

Entao(/^quando eu clicar no botao Logout devera ser apresentado a mensagem "([^"]*)"$/) do |menLogout|
	@navegador.find_element(:css, 'html.no-js body div.row div#content.large-12.columns div.example a.button.secondary.radius i.icon-2x.icon-signout').click
	mensagemLogout = @navegador.find_element(:id, 'flash').text
	expect(mensagemLogout)==(menLogout)
	@navegador.quit
end      

# Cenario 2

Dado(/^que eu tenha o link de acesso$/) do
	@navegador = Selenium::WebDriver.for :firefox
	@navegador.manage.window.maximize
	@navegador.manage.timeouts.implicit_wait = 5
	@navegador.get 'https://the-internet.herokuapp.com/login'
end

Quando(/^eu tentar efetuar o login sem preencher os campos username e password$/) do
	@navegador.find_element(:css, 'button[type="submit"]').click
end

Entao(/^o sistema devera apresentar a mensagem "([^"]*)"$/) do |menA|
	mensagemAlertaA = @navegador.find_element(:id, 'flash').text
	expect(mensagemAlertaA)==(menA)
	@navegador.quit
end                                                                   

# Cenario 3

Dado(/^o link de acesso$/) do                                                                
  @navegador = Selenium::WebDriver.for :firefox
	@navegador.manage.window.maximize
	@navegador.manage.timeouts.implicit_wait = 5
	@navegador.get 'https://the-internet.herokuapp.com/login'                
end                                                                                          
                                                                                             
Quando(/^eu tentar efetuar o login, apos preencher apenas o campo username "([^"]*)"$/) do |user2|
	@navegador.find_element(:id, 'username').send_keys(user2)
	@navegador.find_element(:css, 'button[type="submit"]').click
end         
                                                 
Entao(/^o sistema devera mostrar a mensagem "([^"]*)"$/) do |menB|

---------------------------------------------------------------------------------

Criar um arquivo env.rb na pasta support com os requires

# enconding: utf-8

require 'cucumber/formatter/unicode'
require 'rspec/expectations'
require 'selenium-webdriver'

---------------------------------------------------------------------------------
Preparacao do Ambiente: Exercicio 2
---------------------------------------------------------------------------------

Acessar C:/

Executar: Code

Executar: cd code
 
Executar: md Cucumber_HTP - Cria o diretorio de testes cucumber

Executar: cd Cucumber_GTP - Acessa o diretorio C:/Code/Cucumber_HTP

Executar: gem install cucumber - Instala o Cucumber

Executar: cucumber --init -Cria os diretorios para execucao de testes do cucumber

Executar: bundle init - Cria o Gemfile no diretorio

Criar um arquivo env.rb na pasta support com os requires

# enconding: utf-8

require 'cucumber/formatter/unicode'
require 'rspec/expectations'
require 'selenium-webdriver'

----------------------------------------------------------------------------------

Dentro do Gemfile adicionar as seguintes dependencias:

# enconding: utf-8

require 'cucumber/formatter/unicode'
require 'rspec/expectations'
require 'selenium-webdriver'

----------------------------------------------------------------------------------

Criar uma feature dento do projeto > pasta feature

# language: pt

Funcionalidade: efetuar a busca de um CEP no site do Correio
	Sendo que eu tenha a URL necessario para a busca
	Posso posso criar uma busca 
	 Para conseguir os dados do CEP informado
	
# Cenario 1

	Cenario: Validar a busca de um CEP valido
	   Dado o CEP "06381090"
     Quando a busca no site do correio for efetuada
		  E o CEP for valido
      Entao o sistema devera retornar os dados do CEP informado
	  
# Cenario 2

	Cenario: Validar a busca de um CEP invalido
	   Dado CEP "06381999"
     Quando a busca for relizada no site
		  E o CEP for invalido
      Entao o sistema devera retornar uma mensagem de erro

--------------------------------------------------------------------------------------------

Criar um arquivo .rb na pasta Steps_Definitions

# encoding: utf-8

# Cenario 1

Dado(/^o CEP "([^"]*)"$/) do |dadoBusca|  
	@Cep = dadoBusca
	end                                                                          
                                                                             
Quando(/^a busca no site do correio for efetuada$/) do                       
    @navegador = Selenium::WebDriver.for :firefox
	@navegador.manage.window.maximize
	@navegador.manage.timeouts.implicit_wait = 5
	@correio = 'http://correiosapi.apphb.com/cep/'+@Cep
end                                                                          
                                                                             
Quando(/^o CEP for valido$/) do                                              
	@navegador.get @correio
end                                                                          
                                                                             
Entao(/^o sistema devera retornar os dados do CEP informado$/) do            
	@navegador.find_element(:id, 'tab-0').click
	cepValido = @navegador.find_element(:css, 'html.theme-light body div#content div.tabs div.panels div#panel-0.tab-panel-box div.tab-panel div.jsonPanelBox div.panelContent table.treeTable tbody tr.treeRow.stringRow td.treeValueCell.stringCell span span.objectBox.objectBox-string').text
	expect(cepValido)==(@Cep)
end  

# Cenario 2

Dado(/^CEP "([^"]*)"$/) do |dadoBusca2|
	@cepBusca = dadoBusca2
end
Quando(/^a busca for relizada no site$/) do                                  
  @navegador = Selenium::WebDriver.for :firefox
	@navegador.manage.window.maximize
	@navegador.manage.timeouts.implicit_wait = 5
end                                                                          

Quando(/^o CEP for invalido$/) do                                                                                                                    
	@correio = 'http://correiosapi.apphb.com/cep/'+@cepBusca
end                                                                          
                                                                             
Entao(/^o sistema devera retornar uma mensagem de erro$/) do                 
	@navegador.get @correio
end         

-----------------------------------------------------------------------------------

Criar um arquivo env.rb na pasta support com os requires

# enconding: utf-8

require 'cucumber/formatter/unicode'
require 'rspec/expectations'
require 'selenium-webdriver'

----------------------------------------------------------------------------------

Execucao:

Exercicio 1

Acessar: C:/Code/Cucumber_Test

Executar: Cucumber - Para execucao normal no cmder
ou
Executar cucumber --format html --out relatorio.html - Cria um arquivo HTML com os resultados dos testes

Exercicio 2

Acessar: C:/Code/Cucumber_HTP

Executar: Cucumber - Para execucao normal no cmder
ou
Executar cucumber --format html --out relatorio.html - Cria um arquivo HTML com os resultados dos testes


Zip do projeto: https://1drv.ms/u/s!Aj3ghyvdTl4niXyolCDxHwvTNdXH